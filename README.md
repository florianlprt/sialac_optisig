# SIALAC Optisig

Algorithms for signal controls optimization.

## Setup

Use `venv` (https://gitlab.com/juliendehos/venv)

## Install

```sh
venv i optisig
venv a optisig
pip install pkgconfig setuptools
pip install . --upgrade
```

## Usage

### Run

```sh
./run.sh <scenario> <agents> <home> <activity> <signals> <algo> <algo_params> [logs_dir]
```

### Params

- scenario : `calais` | `quito`
- agents : `5000` | `10000` | `15000` | `20000`
- home : `1h` | `4h` | `uh`
- activity : `1a` | `4a`
- signals : `100` | `75`**\*** | `50`**\***
- algo : `rw` | `hc` | `sa` | `ucb`
- algo params : see below

**\*** *calais scenario only*

#### Algo params

Format : `param1=value1,...,paramN=valueN`.

- general parameters
    - `g` : number of iterations
    - `restarts` : number of restarts
    - `pmops` : mutation operator rates
    - `nmut` : number of mutations per iteration


- *simulated annealing* parameters
    - `t` : initial temperature
    - `steps` : number of steps


- *ucb* parameters
    - `k` : exporation parameter
    - `grouping` : signal systems groups (seen by UCB)
    - `init_evals` : initial evaluations by group

### Examples

```sh
venv optisig
cd sialac_optisig/run/

# calais scenario, 5000_1h_1a plans, 100% signals systems,
# random walk, 10*50 iterations and 1 mutation per iteration
./run.sh calais 5000 1h 1a 50 rw g=50,restarts=10,nmut=1,nsys=33
```

## OAR

### Install

```sh
rm -rf ~/opt/venvs/optisig
virtualenv -p python3 --no-site-packages ~/opt/venvs/optisig
. ~/opt/venvs/optisig/bin/activate
pip install pkgconfig setuptools
pip install . --upgrade
```

### Run

```sh
cd oar
oarsub -S ./oar.sh
```
