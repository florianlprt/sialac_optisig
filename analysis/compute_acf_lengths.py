#!/usr/bin/env python3
''' compute mean acf length '''

import sys
import glob
import pandas
import numpy as np

from statsmodels.tsa.stattools import acf

def acf_threshold(csv):
    ''' autocorrelation and significative threshold '''
    df = pandas.read_csv(csv, sep=';')
    a = acf(df.score)
    t = 2 / len(df.score)**0.5
    return a, t

def compute_mean_length(indir):
    ''' compute mean autocorrelation length '''
    l = 0
    infiles = glob.glob(indir + '*.csv')
    for _ in infiles:
        a, t = acf_threshold(_)
        l += np.argmax(a <= t)
    return l / len(infiles)

if __name__ == '__main__':
    # args
    INPUT_DIR = sys.argv[1]
    # compute
    LENGTH = compute_mean_length(INPUT_DIR)
    # output
    print(LENGTH)
