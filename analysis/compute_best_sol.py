#!/usr/bin/env python3
''' plot algo perfs '''

import sys
import glob
import pandas
import numpy as np

def get_tail(csv):
    df = pandas.read_csv(csv, sep=';')
    return df.tail(1)

def compute_best_sol(input_dir):
    tails = pandas.concat(
        [get_tail(_) for _ in glob.glob(input_dir + '*.csv')],
        ignore_index=True)
    return tails.loc[tails['score'].idxmin()]

if __name__ == '__main__':
    # args
    INPUT_DIR = sys.argv[1]
    # reference
    BEST = compute_best_sol(INPUT_DIR)
    TO_STR = lambda df: '{};{};{};{}'.format(
        df.cycles, df.offsets, df.phases0, df.phases1)
    print(TO_STR(BEST))
