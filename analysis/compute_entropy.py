#!/usr/bin/env python3
''' vassilev plot and metrics '''

import sys
import glob
import math
import pandas
import numpy as np
import matplotlib.pyplot as plt

def vassilev_str(d, ε):
    ''' vassilev string '''
    return '-1' if d < -ε else '0' if d <= ε else '1'

def vassilev_p(vassilev):
    ''' frequencies of sub-blocks '''
    p = {}
    n = 1 / (len(vassilev) - 1)
    for first, second in zip(vassilev, vassilev[1:]):
        key = first + second
        p[key] = p[key] + n if key in p else n
    return p

def fem(p):
    ''' first entropic measure '''
    return -sum([
        val * math.log(val, 6)
        for key, val in p.items()
        if key not in ['-1-1', '00', '11']
    ])

def sem(p):
    ''' second entropic measure '''
    return -sum([
        val * math.log(val, 3)
        for key, val in p.items()
        if key in ['-1-1', '00', '11']
    ])

if __name__ == '__main__':
    # args
    INPUT_DIRS = sys.argv[1:]
    # compute
    FEM_L = []
    SEM_L = []
    E = np.arange(0, 3, 0.1)
    for ε in E:
        print(ε)
        FEM = []
        SEM = []
        for indir in INPUT_DIRS:
            for _ in glob.glob(indir + '*.csv'):
                DF = pandas.read_csv(_, sep=';')
                VASSILEV = [vassilev_str(d, ε) for d in DF.score.diff()[1:]]
                P = vassilev_p(VASSILEV)
                FEM.append(fem(P))
                SEM.append(sem(P))
        FEM_L.append(np.mean(FEM))
        SEM_L.append(np.mean(SEM))
    # out
    print(max(FEM_L))
    # plot
    FIG = plt.figure(dpi=100, figsize=(7, 3))
    AX = FIG.add_subplot(111)
    AX.plot(E, FEM_L, label='H(ε)')
    AX.plot(E, SEM_L, '--', label='h(ε)')
    AX.set_xlabel('ε')
    AX.legend()
    AX.grid()
    FIG.savefig('out.pdf', dpi=100, bbox_inches='tight')
