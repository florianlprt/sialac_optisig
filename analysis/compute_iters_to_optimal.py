#!/usr/bin/env python3
''' plot algo perfs 2 '''

import sys
import glob
import pandas as pd

from scipy import stats

def csv_to_dataframe(csv):
    ''' csv to dataframe '''
    frame = pd.read_csv(csv, sep=';')
    return frame[['generation', 'score']]

if __name__ == '__main__':
    # args
    ALGO_1 = sys.argv[1]
    ALGO_2 = sys.argv[2]
    # dataframes
    DF1 = [csv_to_dataframe(csv) for csv in glob.glob(ALGO_1 + '*.csv')]
    DF2 = [csv_to_dataframe(csv) for csv in glob.glob(ALGO_2 + '*.csv')]
    ALLDF = DF1 + DF2
    DF1 = pd.concat(DF1, ignore_index=True)
    DF2 = pd.concat(DF2, ignore_index=True)
    ALLDF = pd.concat(ALLDF, ignore_index=True)
    # mean best score
    BEST = ALLDF.groupby(['generation']).score.mean().values[-1] * 1.1
    DF1_GRP = DF1.groupby(['generation']).score.mean()
    DF2_GRP = DF2.groupby(['generation']).score.mean()
    # stats
    EVALS1 = DF1_GRP[DF1_GRP <= BEST].index[0]
    EVALS2 = DF2_GRP[DF2_GRP <= BEST].index[0]
    RATIO = 1 - EVALS2 / EVALS1
    TEST1 = DF1[DF1['generation'] == EVALS2]
    TEST2 = DF2[DF2['generation'] == EVALS2]
    # out
    print(EVALS1, EVALS2, RATIO)
    print(stats.mannwhitneyu(TEST1.score.values, TEST2.score.values))
