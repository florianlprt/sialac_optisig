#!/usr/bin/env python3
''' mutual information metrics '''

import sys
import glob
import pandas
import numpy as np
import itertools as it

UP = '1'
DOWN = '-1'

def f(s, l):
    ''' frequencies of sub-blocks of length l in string s '''
    n = len(s) - 2 * (l - 1) - 1
    f = 1 / n
    d = {}
    for i in range(n):
        first = ''.join(s[i:i+l])
        second = ''.join(s[i+l:i+2*l])
        key = first + second
        d[first] = d[first] + f if first in d else f
        d[key] = d[key] + f if key in d else f
    return d

def mut_inf(f, x, y):
    ''' mutual information '''
    x = ''.join(x)
    y = ''.join(y)
    return f[x+y] * np.log(f[x+y] / (f[x] * f[y])) if x in f and y in f and x+y in f else 0

if __name__ == '__main__':
    # args
    L = int(sys.argv[1])
    INPUT_DIRS = sys.argv[2:]
    # compute
    I = []
    F_LIST = []
    for indir in INPUT_DIRS:
        for _ in glob.glob(indir + '*.csv'):
            DF = pandas.read_csv(_, sep=';')
            S = [UP if d >=0 else DOWN for d in DF.score.diff()[1:]]
            F = f(S, L)
            I.append(sum([
                mut_inf(F, x, y)
                for x, y in it.product(it.product([DOWN,UP], repeat=L), repeat=2)
            ]))
            F_LIST.append(F)
    # out
    DF = pandas.DataFrame(F_LIST)
    I = np.array(I)
    print(DF.mean())
    print(I.mean())
