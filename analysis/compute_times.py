#!/usr/bin/env python3
''' plot algo times '''

import sys
import glob
import pandas as pd

def csv_to_dataframe(csv):
    ''' csv to dataframe '''
    frame = pd.read_csv(csv, sep=';')
    return frame[['time', 'simtime']]

if __name__ == '__main__':
    # args
    ALGO1 = sys.argv[1]
    ALGO2 = sys.argv[2]
    ALGO3 = sys.argv[3]
    ALGO4 = sys.argv[4]
    # data
    CSV1 = glob.glob(ALGO1 + '*.csv')[0]
    CSV2 = glob.glob(ALGO2 + '*.csv')[0]
    CSV3 = glob.glob(ALGO3 + '*.csv')[0]
    CSV4 = glob.glob(ALGO4 + '*.csv')[0]
    DF1 = csv_to_dataframe(CSV1)
    DF2 = csv_to_dataframe(CSV2)
    DF3 = csv_to_dataframe(CSV3)
    DF4 = csv_to_dataframe(CSV4)
    TOTAL1 = DF1['time'].diff()[1:]
    TOTAL2 = DF2['time'].diff()[1:]
    TOTAL3 = DF3['time'].diff()[1:]
    TOTAL4 = DF4['time'].diff()[1:]
    SIM1 = DF1['simtime'][1:]
    SIM2 = DF2['simtime'][1:]
    SIM3 = DF3['simtime'][1:]
    SIM4 = DF4['simtime'][1:]
    # tex
    print('        \\multirow{{6}}{{*}}{{5000}} & \\multirow{{2}}{{*}}{{1}} & 1 & {:.3} s & {:.2E} & {:.2E} \\\\'.format(
        (SIM1.append(SIM2)).mean(), (TOTAL1-SIM1).mean(), (TOTAL2-SIM2).mean()
    ))
    print('                              &                    & 4 & {:.3} s & {:.2E} & {:.2E} \\\\'.format(
        (SIM3.append(SIM4)).mean(), (TOTAL3-SIM3).mean(), (TOTAL4-SIM4).mean()
    ))
    print('                              \\cline{2-3}')

# \multirow{6}{*}{5000} & \multirow{2}{*}{1} & 1 & & & \\
#                       &                    & 4 & & & \\
#                       \cline{2-3}
#                       & \multirow{2}{*}{4} & 1 & & & \\
#                       &                    & 4 & & & \\
#                       \cline{2-3}
#                       & \multirow{2}{*}{uniform} & 1 & & & \\
#                       &                          & 4 & & & \\
