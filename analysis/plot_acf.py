#!/usr/bin/env python3
''' plot autocorrelation '''


import sys
import pandas
import matplotlib.pyplot as plt

from statsmodels.tsa.stattools import acf

if __name__ == '__main__':
    # args
    INPUT_FILE = sys.argv[1]
    # compute
    DF = pandas.read_csv(INPUT_FILE, sep=';')
    ACF = acf(DF.score)
    THRESHOLD = 2 / len(DF.score)**0.5
    # plot
    FIG = plt.figure(dpi=100, figsize=(8, 8))
    AX = FIG.add_subplot(111)
    AX.bar(range(len(ACF)), ACF, width=0.2)
    AX.plot([THRESHOLD] * len(ACF), 'C1--')
    AX.set_ylabel('Autocorrelation')
    AX.set_xlabel('Lag')
    AX.grid()
    FIG.savefig('out.pdf', dpi=100, bbox_inches='tight')
