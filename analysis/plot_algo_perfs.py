#!/usr/bin/env python3
''' plot algo perfs '''

import sys
import glob
import pandas
import numpy as np
import matplotlib.pyplot as plt

START = 10
END = -1

def read_scores(csv):
    _ = pandas.read_csv(csv, sep=';')
    return _.score

def compute_perfs(input_dir):
    scores = np.array([
        read_scores(_)
        for _ in glob.glob(input_dir + '*.csv')
    ])
    means = scores.mean(axis=0)
    stds = scores.std(axis=0)
    confidences = (1.6 * stds) / np.sqrt(len(scores))
    return means[START:END], confidences[START:END]

if __name__ == '__main__':
    # args
    XLABEL = sys.argv[1]
    YLABEL = sys.argv[2]
    INPUT_DIRS = sys.argv[3:]
    # labels (ugly)
    SCENARIO = '-'.join(INPUT_DIRS[0].split('_')[6:])[:-1]
    # R_GEO = INPUT_DIRS[1].split('_')[-1][1:-1]
    # R_IMP = INPUT_DIRS[2].split('_')[-1][1:-1]

    LABELS = ['hc', 'ucb imp.']
    # LABELS = ['descente de gradient', 'algorithme adaptatif']
    # LABELS = ['hc', 'ucb geo.', 'ucb imp.']
    # LABELS = ['hc', 'ucb (R = 0.1)', 'ucb (R = 2.75)', 'ucb (R = 100)']
    # LABELS = ['hc (init. random)', 'hc (init. from best 1h-4a)', 'hc (init. from best 4h-1a)']

    LINES = [':', '-']
    # LINES = [':', '--', '-']
    # LINES = [':', '--', '-', '-.']
    # LINES = [':', '--', '-']

    # FILENAME = 'out_{}.pdf'.format(SCENARIO)
    # plot
    FIG = plt.figure(dpi=100, figsize=(4, 4))
    AX = FIG.add_subplot(111)
    for _, ls, label in zip(INPUT_DIRS, LINES, LABELS):
    # for _ in INPUT_DIRS:
        MEANS, CONFIDENCES = compute_perfs(_)
        X = range(START, len(MEANS)+START)
        # AX.plot(X, MEANS, lw=1.7)
        AX.plot(X, MEANS, lw=1.7, ls=ls, label=label)
        AX.fill_between(X, MEANS-CONFIDENCES, MEANS+CONFIDENCES, alpha=.33)
    if (XLABEL):
        AX.set_xlabel('Evaluations')
        # AX.set_xlabel('Itérations')
    if (YLABEL):
        AX.set_ylabel('Mean trip duration (s.)')
        # AX.set_ylabel('Temps de parcours moyen (sec.)')
    AX.set_xscale('log')
    # AX.plot(range(START, 2000), [873.5]*1990, '--', color='grey', label='baseline reference')

    # AX.legend(loc=3).set_title('Instance: {}'.format(SCENARIO))
    AX.legend(loc=1).set_title('Instance: {}'.format(SCENARIO))
    # AX.legend(loc=3)

    AX.grid()
    FIG.savefig('out.pdf', format='pdf', bbox_inches='tight')
    # FIG.savefig('out_cross-sol.pdf', format='pdf', bbox_inches='tight')
    # FIG.savefig(FILENAME, format='pdf', bbox_inches='tight')
