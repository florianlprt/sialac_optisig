#!/bin/sh

##########################
# PLOTS
##########################

PLT=$1

./plot_algo_perfs.py '' 'True' $PLT/hc_calais_5000_1h_1a/ $PLT/ucb_calais_5000_1h_1a_geo_k*/ $PLT/ucb_calais_5000_1h_1a_k1/
./plot_algo_perfs.py '' '' $PLT/hc_calais_5000_1h_4a/ $PLT/ucb_calais_5000_1h_4a_geo_k*/ $PLT/ucb_calais_5000_1h_4a_k*/
./plot_algo_perfs.py '' '' $PLT/hc_calais_5000_4h_1a/ $PLT/ucb_calais_5000_4h_1a_geo_k*/ $PLT/ucb_calais_5000_4h_1a_k*/
./plot_algo_perfs.py 'True' 'True' $PLT/hc_calais_5000_4h_4a/ $PLT/ucb_calais_5000_4h_4a_geo_k*/ $PLT/ucb_calais_5000_4h_4a_k*/
./plot_algo_perfs.py 'True' '' $PLT/hc_calais_5000_uh_1a/ $PLT/ucb_calais_5000_uh_1a_geo_k*/ $PLT/ucb_calais_5000_uh_1a_k*/
./plot_algo_perfs.py 'True' '' $PLT/hc_calais_5000_uh_4a/ $PLT/ucb_calais_5000_uh_4a_geo_k*/ $PLT/ucb_calais_5000_uh_4a_k*/

##########################
# PLOTS reboot
##########################

PLT='/home/flo/Depots/sialac_results/optim/calais_5000_perfs_asoc2'
PLT1='/home/flo/Depots/sialac_results/optim/calais_5000_perfs'
# PLT2='/home/flo/Depots/sialac_results/optim/calais_5000_perfs_reboot'

# ./plot_algo_perfs2.py 'Scenario: 1h-1a' '' 'True' $PLT1/hc_calais_5000_1h_1a/ $PLT1/ucb_calais_5000_1h_1a_geo_k1/ $PLT/ucb_calais_5000_1h_1a_imp_k1/ $PLT/ucb_calais_5000_1h_1a_liu_k1_liu/ $PLT/eea_calais_5000_1h_1a/
# ./plot_algo_perfs2.py 'Scenario: 1h-4a' '' '' $PLT/hc_calais_5000_1h_4a/ $PLT/ucb_calais_5000_1h_4a_geo_k1/ $PLT/ucb_calais_5000_1h_4a_imp_k1/ $PLT/ucb_calais_5000_1h_4a_liu_k1_liu/ $PLT/eea_calais_5000_1h_4a/
# ./plot_algo_perfs2.py 'Scenario: 4h-1a' '' '' $PLT1/hc_calais_5000_4h_1a/ $PLT1/ucb_calais_5000_4h_1a_geo_k1/ $PLT/ucb_calais_5000_4h_1a_imp_k1_liu/ $PLT/ucb_calais_5000_4h_1a_liu_k1_liu/ $PLT/eea_calais_5000_4h_1a/
# ./plot_algo_perfs2.py 'Scenario: 4h-4a' 'True' 'True' $PLT/hc_calais_5000_4h_4a/ $PLT/ucb_calais_5000_4h_4a_geo_k1/ $PLT/ucb_calais_5000_4h_4a_imp_k1/ $PLT/ucb_calais_5000_4h_4a_liu_k1_liu/ $PLT/eea_calais_5000_4h_4a/
# ./plot_algo_perfs2.py 'Scenario: uh-1a' 'True' '' $PLT1/hc_calais_5000_uh_1a/ $PLT1/ucb_calais_5000_uh_1a_geo_k1/ $PLT/ucb_calais_5000_uh_1a_imp_k1/ $PLT/ucb_calais_5000_uh_1a_liu_k1_liu/ $PLT/eea_calais_5000_uh_1a/
# ./plot_algo_perfs2.py 'Scenario: uh-4a' 'True' '' $PLT/hc_calais_5000_uh_4a/ $PLT/ucb_calais_5000_uh_4a_geo_k1/ $PLT/ucb_calais_5000_uh_4a_imp_k1_liu/ $PLT/ucb_calais_5000_uh_4a_liu_k1_liu/ $PLT/eea_calais_5000_uh_4a/
./plot_algo_perfs2.py 'Scenario: 1h-1a' '' 'True' $PLT/hc_calais_5000_1h_1a/ $PLT/ucb_calais_5000_1h_1a_geo_r2/ $PLT/ucb_calais_5000_1h_1a_imp_r2/ $PLT/ucb_calais_5000_1h_1a_liu_k1_liu/ $PLT/eea_calais_5000_1h_1a/
cp out.pdf /home/flo/Depots/sialac_asoc/sources/figures/perfs_calais_1h_1a.pdf
./plot_algo_perfs2.py 'Scenario: 1h-4a' '' '' $PLT/hc_calais_5000_1h_4a/ $PLT/ucb_calais_5000_1h_4a_geo_r2/ $PLT/ucb_calais_5000_1h_4a_imp_r2/ $PLT/ucb_calais_5000_1h_4a_liu_k1_liu/ $PLT/eea_calais_5000_1h_4a/
cp out.pdf /home/flo/Depots/sialac_asoc/sources/figures/perfs_calais_1h_4a.pdf
./plot_algo_perfs2.py 'Scenario: 4h-1a' '' '' $PLT/hc_calais_5000_4h_1a/ $PLT/ucb_calais_5000_4h_1a_geo_r2/ $PLT/ucb_calais_5000_4h_1a_imp_r2/ $PLT/ucb_calais_5000_4h_1a_liu_k1_liu/ $PLT/eea_calais_5000_4h_1a/
cp out.pdf /home/flo/Depots/sialac_asoc/sources/figures/perfs_calais_4h_1a.pdf
./plot_algo_perfs2.py 'Scenario: 4h-4a' 'True' 'True' $PLT/hc_calais_5000_4h_4a/ $PLT/ucb_calais_5000_4h_4a_geo_r2/ $PLT/ucb_calais_5000_4h_4a_imp_r2/ $PLT/ucb_calais_5000_4h_4a_liu_k1_liu/ $PLT/eea_calais_5000_4h_4a/
cp out.pdf /home/flo/Depots/sialac_asoc/sources/figures/perfs_calais_4h_4a.pdf
./plot_algo_perfs2.py 'Scenario: uh-1a' 'True' '' $PLT1/hc_calais_5000_uh_1a/ $PLT1/ucb_calais_5000_uh_1a_geo_k1/ $PLT/ucb_calais_5000_uh_1a_imp_r2/ $PLT/ucb_calais_5000_uh_1a_liu_k1_liu/ $PLT/eea_calais_5000_uh_1a/
cp out.pdf /home/flo/Depots/sialac_asoc/sources/figures/perfs_calais_uh_1a.pdf
./plot_algo_perfs2.py 'Scenario: uh-4a' 'True' '' $PLT/hc_calais_5000_uh_4a/ $PLT/ucb_calais_5000_uh_4a_geo_r2/ $PLT/ucb_calais_5000_uh_4a_imp_r2/ $PLT/ucb_calais_5000_uh_4a_liu_k1_liu/ $PLT/eea_calais_5000_uh_4a/
cp out.pdf /home/flo/Depots/sialac_asoc/sources/figures/perfs_calais_uh_4a.pdf

## FOR SUBMISSION ONLY
./plot_algo_perfs2.py 'Scenario: 1h-1a' '' 'True' $PLT/hc_calais_5000_1h_1a/ $PLT/ucb_calais_5000_1h_1a_geo_r2/ $PLT/ucb_calais_5000_1h_1a_imp_r2/ $PLT/ucb_calais_5000_1h_1a_liu_k1_liu/ $PLT/eea_calais_5000_1h_1a/
cp out.pdf /home/flo/Depots/sialac_asoc/submissions/submission2/perfs_calais_1h_1a.pdf
./plot_algo_perfs2.py 'Scenario: 1h-4a' '' '' $PLT/hc_calais_5000_1h_4a/ $PLT/ucb_calais_5000_1h_4a_geo_r2/ $PLT/ucb_calais_5000_1h_4a_imp_r2/ $PLT/ucb_calais_5000_1h_4a_liu_k1_liu/ $PLT/eea_calais_5000_1h_4a/
cp out.pdf /home/flo/Depots/sialac_asoc/submissions/submission2/perfs_calais_1h_4a.pdf
./plot_algo_perfs2.py 'Scenario: 4h-1a' '' 'True' $PLT/hc_calais_5000_4h_1a/ $PLT/ucb_calais_5000_4h_1a_geo_r2/ $PLT/ucb_calais_5000_4h_1a_imp_r2/ $PLT/ucb_calais_5000_4h_1a_liu_k1_liu/ $PLT/eea_calais_5000_4h_1a/
cp out.pdf /home/flo/Depots/sialac_asoc/submissions/submission2/perfs_calais_4h_1a.pdf
./plot_algo_perfs2.py 'Scenario: 4h-4a' '' '' $PLT/hc_calais_5000_4h_4a/ $PLT/ucb_calais_5000_4h_4a_geo_r2/ $PLT/ucb_calais_5000_4h_4a_imp_r2/ $PLT/ucb_calais_5000_4h_4a_liu_k1_liu/ $PLT/eea_calais_5000_4h_4a/
cp out.pdf /home/flo/Depots/sialac_asoc/submissions/submission2/perfs_calais_4h_4a.pdf
./plot_algo_perfs2.py 'Scenario: uh-1a' 'True' 'True' $PLT1/hc_calais_5000_uh_1a/ $PLT1/ucb_calais_5000_uh_1a_geo_k1/ $PLT/ucb_calais_5000_uh_1a_imp_r2/ $PLT/ucb_calais_5000_uh_1a_liu_k1_liu/ $PLT/eea_calais_5000_uh_1a/
cp out.pdf /home/flo/Depots/sialac_asoc/submissions/submission2/perfs_calais_uh_1a.pdf
./plot_algo_perfs2.py 'Scenario: uh-4a' 'True' '' $PLT/hc_calais_5000_uh_4a/ $PLT/ucb_calais_5000_uh_4a_geo_r2/ $PLT/ucb_calais_5000_uh_4a_imp_r2/ $PLT/ucb_calais_5000_uh_4a_liu_k1_liu/ $PLT/eea_calais_5000_uh_4a/
cp out.pdf /home/flo/Depots/sialac_asoc/submissions/submission2/perfs_calais_uh_4a.pdf

PLT3='/home/flo/Depots/sialac_results/optim/quito_20000_perfs_reboot'
./plot_algo_perfs2.py 'Scenario: 1h-1a' 'True' 'True' $PLT3/hc_quito_20000_1h_1a/ $PLT3/ucb_quito_20000_1h_1a_imp_k1/
cp out.pdf /home/flo/Depots/sialac_asoc/sources/figures/perfs_quito_1h_1a.pdf
./plot_algo_perfs2.py 'Scenario: 4h-4a' 'True' '' $PLT3/hc_quito_20000_4h_4a/ $PLT3/ucb_quito_20000_4h_4a_imp_k1/
cp out.pdf /home/flo/Depots/sialac_asoc/sources/figures/perfs_quito_4h_4a.pdf

./compute_ttest.py $PLT1/hc_calais_5000_1h_1a/ $PLT/ucb_calais_5000_1h_1a_imp_r2/
./compute_ttest.py $PLT/hc_calais_5000_1h_4a/ $PLT/ucb_calais_5000_1h_4a_imp_r2/
./compute_ttest.py $PLT1/hc_calais_5000_4h_1a/ $PLT/ucb_calais_5000_4h_1a_imp_r2/
./compute_ttest.py $PLT/hc_calais_5000_4h_4a/ $PLT/ucb_calais_5000_4h_4a_imp_r2/
./compute_ttest.py $PLT1/hc_calais_5000_uh_1a/ $PLT/ucb_calais_5000_uh_1a_imp_r2/
./compute_ttest.py $PLT/hc_calais_5000_uh_4a/ $PLT/ucb_calais_5000_uh_4a_imp_r2/

./compute_iters_to_optimal.py $PLT/hc_calais_5000_1h_1a/ $PLT/ucb_calais_5000_1h_1a_imp_r2/
./compute_iters_to_optimal.py $PLT/hc_calais_5000_1h_4a/ $PLT/ucb_calais_5000_1h_4a_imp_r2/
./compute_iters_to_optimal.py $PLT/hc_calais_5000_4h_1a/ $PLT/ucb_calais_5000_4h_1a_imp_r2/
./compute_iters_to_optimal.py $PLT/hc_calais_5000_4h_4a/ $PLT/ucb_calais_5000_4h_4a_imp_r2/
./compute_iters_to_optimal.py $PLT1/hc_calais_5000_uh_1a/ $PLT/ucb_calais_5000_uh_1a_imp_r2/
./compute_iters_to_optimal.py $PLT/hc_calais_5000_uh_4a/ $PLT/ucb_calais_5000_uh_4a_imp_r2/

PLT4='/home/flo/Depots/sialac_results/optim/calais_5000_explo'
./plot_algo_perfs2.py 'Scenario: uh-1a' 'True' 'True' $PLT1/hc_calais_5000_uh_1a/ $PLT4/ucb_calais_5000_uh_1a_k0.1/ $PLT4/ucb_calais_5000_uh_1a_k2.75/ $PLT4/ucb_calais_5000_uh_1a_k100/
cp out.pdf /home/flo/Depots/sialac_asoc/sources/figures/exploration_uh_1a.pdf

PLT5='/home/flo/Depots/sialac_results/optim/calais_5000_cross/'
./plot_algo_perfs2.py 'Scenario: uh-1a' 'True' 'True' $PLT1/hc_calais_5000_uh_1a/ $PLT5/hc_calais_5000_uh_1a_from_1h_4a/ $PLT5/hc_calais_5000_uh_1a_from_4h_1a/

PLT6='/home/flo/Depots/sialac_results/optim/quito_20000_perfs_asoc2/'
./plot_algo_perfs2.py 'Scenario: 1h-1a' 'True' 'True' $PLT6/hc_quito_20000_1h_1a/ $PLT6/ucb_quito_20000_1h_1a_imp_r0.5/
cp out.pdf ~/Depots/sialac_asoc/sources/figures/perfs_quito_1h_1a.pdf
./plot_algo_perfs2.py 'Scenario: 4h-4a' 'True' '' $PLT6/hc_quito_20000_4h_4a/ $PLT6/ucb_quito_20000_4h_4a_imp_r0.5/
cp out.pdf ~/Depots/sialac_asoc/sources/figures/perfs_quito_4h_4a.pdf

PLT7='/home/flo/Depots/sialac_results/optim/times_asoc2/'
./compute_times.py $PLT7/hc_calais_5000_1h_1a_times/ $PLT7/ucb_calais_5000_1h_1a_imp_r2_times/ $PLT7/hc_calais_5000_1h_4a_times/ $PLT7/ucb_calais_5000_1h_4a_imp_r2_times/
./compute_times.py $PLT7/hc_calais_5000_4h_1a_times/ $PLT7/ucb_calais_5000_4h_1a_imp_r2_times/ $PLT7/hc_calais_5000_4h_4a_times/ $PLT7/ucb_calais_5000_4h_4a_imp_r2_times/
./compute_times.py $PLT7/hc_calais_5000_uh_1a_times/ $PLT7/ucb_calais_5000_uh_1a_imp_r2_times/ $PLT7/hc_calais_5000_uh_4a_times/ $PLT7/ucb_calais_5000_uh_4a_imp_r2_times/
./compute_times.py $PLT7/hc_calais_10000_1h_1a_times/ $PLT7/ucb_calais_10000_1h_1a_imp_r2_times/ $PLT7/hc_calais_10000_1h_4a_times/ $PLT7/ucb_calais_10000_1h_4a_imp_r2_times/
./compute_times.py $PLT7/hc_calais_10000_4h_1a_times/ $PLT7/ucb_calais_10000_4h_1a_imp_r2_times/ $PLT7/hc_calais_10000_4h_4a_times/ $PLT7/ucb_calais_10000_4h_4a_imp_r2_times/
./compute_times.py $PLT7/hc_calais_10000_uh_1a_times/ $PLT7/ucb_calais_10000_uh_1a_imp_r2_times/ $PLT7/hc_calais_10000_uh_4a_times/ $PLT7/ucb_calais_10000_uh_4a_imp_r2_times/
./compute_times.py $PLT7/hc_calais_15000_1h_1a_times/ $PLT7/ucb_calais_15000_1h_1a_imp_r2_times/ $PLT7/hc_calais_15000_1h_4a_times/ $PLT7/ucb_calais_15000_1h_4a_imp_r2_times/
./compute_times.py $PLT7/hc_calais_15000_4h_1a_times/ $PLT7/ucb_calais_15000_4h_1a_imp_r2_times/ $PLT7/hc_calais_15000_4h_4a_times/ $PLT7/ucb_calais_15000_4h_4a_imp_r2_times/
./compute_times.py $PLT7/hc_calais_15000_uh_1a_times/ $PLT7/ucb_calais_15000_uh_1a_imp_r2_times/ $PLT7/hc_calais_15000_uh_4a_times/ $PLT7/ucb_calais_15000_uh_4a_imp_r2_times/
./compute_times.py $PLT7/hc_calais_20000_1h_1a_times/ $PLT7/ucb_calais_20000_1h_1a_imp_r2_times/ $PLT7/hc_calais_20000_1h_4a_times/ $PLT7/ucb_calais_20000_1h_4a_imp_r2_times/
./compute_times.py $PLT7/hc_calais_20000_4h_1a_times/ $PLT7/ucb_calais_20000_4h_1a_imp_r2_times/ $PLT7/hc_calais_20000_4h_4a_times/ $PLT7/ucb_calais_20000_4h_4a_imp_r2_times/
./compute_times.py $PLT7/hc_calais_20000_uh_1a_times/ $PLT7/ucb_calais_20000_uh_1a_imp_r2_times/ $PLT7/hc_calais_20000_uh_4a_times/ $PLT7/ucb_calais_20000_uh_4a_imp_r2_times/

./compute_times.py $PLT7/hc_quito_5000_1h_1a_times/ $PLT7/ucb_quito_5000_1h_1a_imp_r2_times/ $PLT7/hc_quito_5000_1h_4a_times/ $PLT7/ucb_quito_5000_1h_4a_imp_r2_times/
./compute_times.py $PLT7/hc_quito_5000_4h_1a_times/ $PLT7/ucb_quito_5000_4h_1a_imp_r2_times/ $PLT7/hc_quito_5000_4h_4a_times/ $PLT7/ucb_quito_5000_4h_4a_imp_r2_times/
./compute_times.py $PLT7/hc_quito_5000_uh_1a_times/ $PLT7/ucb_quito_5000_uh_1a_imp_r2_times/ $PLT7/hc_quito_5000_uh_4a_times/ $PLT7/ucb_quito_5000_uh_4a_imp_r2_times/
./compute_times.py $PLT7/hc_quito_10000_1h_1a_times/ $PLT7/ucb_quito_10000_1h_1a_imp_r2_times/ $PLT7/hc_quito_10000_1h_4a_times/ $PLT7/ucb_quito_10000_1h_4a_imp_r2_times/
./compute_times.py $PLT7/hc_quito_10000_4h_1a_times/ $PLT7/ucb_quito_10000_4h_1a_imp_r2_times/ $PLT7/hc_quito_10000_4h_4a_times/ $PLT7/ucb_quito_10000_4h_4a_imp_r2_times/
./compute_times.py $PLT7/hc_quito_10000_uh_1a_times/ $PLT7/ucb_quito_10000_uh_1a_imp_r2_times/ $PLT7/hc_quito_10000_uh_4a_times/ $PLT7/ucb_quito_10000_uh_4a_imp_r2_times/
./compute_times.py $PLT7/hc_quito_15000_1h_1a_times/ $PLT7/ucb_quito_15000_1h_1a_imp_r2_times/ $PLT7/hc_quito_15000_1h_4a_times/ $PLT7/ucb_quito_15000_1h_4a_imp_r2_times/
./compute_times.py $PLT7/hc_quito_15000_4h_1a_times/ $PLT7/ucb_quito_15000_4h_1a_imp_r2_times/ $PLT7/hc_quito_15000_4h_4a_times/ $PLT7/ucb_quito_15000_4h_4a_imp_r2_times/
./compute_times.py $PLT7/hc_quito_15000_uh_1a_times/ $PLT7/ucb_quito_15000_uh_1a_imp_r2_times/ $PLT7/hc_quito_15000_uh_4a_times/ $PLT7/ucb_quito_15000_uh_4a_imp_r2_times/
./compute_times.py $PLT7/hc_quito_20000_1h_1a_times/ $PLT7/ucb_quito_20000_1h_1a_imp_r2_times/ $PLT7/hc_quito_20000_1h_4a_times/ $PLT7/ucb_quito_20000_1h_4a_imp_r2_times/
./compute_times.py $PLT7/hc_quito_20000_4h_1a_times/ $PLT7/ucb_quito_20000_4h_1a_imp_r2_times/ $PLT7/hc_quito_20000_4h_4a_times/ $PLT7/ucb_quito_20000_4h_4a_imp_r2_times/
./compute_times.py $PLT7/hc_quito_20000_uh_1a_times/ $PLT7/ucb_quito_20000_uh_1a_imp_r2_times/ $PLT7/hc_quito_20000_uh_4a_times/ $PLT7/ucb_quito_20000_uh_4a_imp_r2_times/
