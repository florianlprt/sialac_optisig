#!/usr/bin/env python3
''' plot algo perfs 2 '''

import sys
import glob
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

def csv_to_dataframe(csv):
    ''' csv to dataframe '''
    frame = pd.read_csv(csv, sep=';')
    return frame[['generation', 'score']]

def compute_perfs(directory):
    ''' compute perfs '''
    frames = [csv_to_dataframe(csv)
              for csv in glob.glob(directory + '*.csv')]
    result = pd.concat(frames, ignore_index=True)
    group = result.groupby(['generation'])
    means = group.score.mean()
    stds = group.score.std()
    confidences = (1.645 * stds) / np.sqrt(len(frames))
    return result.generation.unique(), means, confidences

if __name__ == '__main__':
    # args
    TITLE = sys.argv[1]
    XLABEL = sys.argv[2]
    YLABEL = sys.argv[3]
    INPUT_DIRS = sys.argv[4:]
    # viz
    MARKERS = ['o', 'v', '^', 's', 'D']
    # MARKERS = ['o', '^']
    # MARKERS = ['o', '^', '^', '^']
    # MARKERS = ['o', 'o', 'o']
    LINES = ['-', '-', '-', '-', '-']
    # LINES = ['-', '-']
    # LINES = ['-', ':', '-', '--']
    # LINES = [':', '--', '-']
    # plot
    FIG = plt.figure(dpi=100, figsize=(4, 4))
    AX = FIG.add_subplot(111)
    for input_dir, m, l in zip(INPUT_DIRS, MARKERS, LINES):
        X, MEANS, CONFIDENCES = compute_perfs(input_dir)
        AX.plot(X, MEANS, marker=m, markevery=0.1, markersize=6, ls=l)
        AX.fill_between(X, MEANS-CONFIDENCES, MEANS+CONFIDENCES, alpha=.33)
    # AX.plot(range(0,2000), [875]*2000, ls='--')
    if XLABEL: AX.set_xlabel('Evaluations')
    if YLABEL: AX.set_ylabel('Mean trip duration (s.)')
    AX.set_xscale('log')
    AX.set_xlim((8, 2000))
    # AX.set_xlim((10, 3900))
    AX.legend(['hc', 'bd geo.', 'bd imp.', 'bd full', 'ea']).set_title(TITLE)
    # AX.legend(['hc', 'bd']).set_title(TITLE)
    # AX.legend(['hc', 'bd ($R=0.1$)', 'bd ($R=2.75$)', 'bd ($R=100$)']).set_title(TITLE)
    # AX.legend(['hc (init. random)', 'hc (init. from best 1h-4a)', 'hc (init. from best 4h-1a)', 'baseline reference']).set_title(TITLE)
    AX.grid()
    FIG.savefig('out.pdf', format='pdf', bbox_inches='tight')
