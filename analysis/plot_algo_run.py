#!/usr/bin/env python3
''' plot algo run '''

import sys
import pandas
import matplotlib.pyplot as plt

if __name__ == '__main__':
    # args
    DF = pandas.read_csv(sys.argv[1], sep=';')
    # plot
    FIG = plt.figure(dpi=100, figsize=(8, 8))
    AX = FIG.add_subplot(111)
    AX.plot(DF.score)
    AX.set_xlabel('Iterations')
    AX.set_ylabel('Mean trip duration (sec)')
    AX.grid()
    FIG.savefig('out.pdf', dpi=100, bbox_inches='tight')
