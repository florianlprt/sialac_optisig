#!/usr/bin/env python3
''' plot signals importance '''

import sys
import glob
import pandas
import numpy as np

import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

# Compute
#########################################

def normalize(fitness):
    ''' normalize fitness '''
    return (fitness - np.mean(fitness)) / np.std(fitness)

def deltas_by_signals(csv, ns):
    ''' deltas of fitness grouped by signals systems '''
    df = pandas.read_csv(csv, sep=';')
    df.score = normalize(df.score)
    deltas = np.zeros(ns)
    for signal, diff in zip(df.mutants[1:], df.score.diff()[1:]):
        deltas[int(signal)] += abs(diff)
    return deltas

def compute_deltas(instances, ns):
    ''' deltas of fitness for each instances '''
    return [
        [deltas_by_signals(_, ns) for _ in glob.glob(_ + '*.csv')]
        for _ in instances
    ]

def compute_threshold(deltas, rate):
    ''' compute the significative threshold to detect important signals '''
    deltas_sum = np.sum(deltas, axis=0)
    deltas_sum = normalize(deltas_sum)
    return np.percentile(deltas_sum, rate)

def compute_groups(importances):
    ''' compute groups for ucb algorithm '''
    critical, moderate, neutral = [], [], []
    for n, imp in enumerate(importances):
        _ = critical if imp > 2 else moderate if imp > 1 else neutral
        _.append(n)
    return [critical] + [moderate] + [neutral]

def make_dataframe(instances, importances):
    ''' make dataframes of features and importances '''
    labels = [_.split('/')[-2].split('_') for _ in instances]
    n = [_[2] for _ in labels]
    h = [_[3] for _ in labels]
    a = [_[4] for _ in labels]
    return pandas.DataFrame([
        {'n': '05000' if n == '5000' else n, 'h': h, 'a': a, 'imp': imp}
        for n, h, a, imp in zip(n, h, a, importances)
    ])

# Plots
#########################################

def plot_importance(fig, ax, df):
    ''' plot all signals importance as heatmap '''
    df = df[df.n.isin(['05000', '10000', '15000', '20000'])]
    labels = ['{}_{}_{}'.format(n, h, a) for n, h, a in zip(df.n, df.h, df.a)]
    cax = ax.matshow(df.imp.tolist(), cmap='YlOrRd')
    ax.set_xlabel('Traffic light id.')
    ax.xaxis.set_ticks_position('bottom')
    ax.yaxis.set_major_locator(ticker.MultipleLocator(1))
    ax.set_yticklabels([''] + labels)
    fig.colorbar(cax, fraction=0.01, pad=0.03).set_label('# significative δ')

def plot_importance_group_by(ax, df, feature):
    ''' plot signals importance by feature '''
    df = (DF.groupby(feature).agg({'imp': lambda x: list(x.mean())}))
    n = len(df.index)
    w = (1 / n) * 0.8
    for _, (idx, row) in enumerate(df.iterrows()):
        x = (np.arange(len(row.imp)) + w * (_ - (n-w)/2))
        ax.bar(x, row.imp, width=w, label=idx)
    ax.set_xlim((-0.8, 69.8))
    ax.xaxis.set_minor_locator(ticker.MultipleLocator(1))
    ax.yaxis.grid()
    ax.legend().set_title('Activity clusters')
    ax.set_xlabel('Signal system id.')
    ax.set_ylabel('Mean of importance')

if __name__ == '__main__':
    # args
    RATE = int(sys.argv[1])
    NB_SIGNALS = int(sys.argv[2])
    INSTANCES = sys.argv[3:]
    # compute
    DELTAS = compute_deltas(INSTANCES, NB_SIGNALS)
    THRESHOLD = compute_threshold(DELTAS, RATE)
    IMPORTANCES = [np.sum(_ > np.float64(THRESHOLD), axis=0) for _ in DELTAS]
    # dataframes
    DF = make_dataframe(INSTANCES, IMPORTANCES)
    # plots
    FIG = plt.figure(dpi=100, figsize=(8, 3))
    AX = FIG.add_subplot(111)
    # plot_importance(FIG, AX, DF)
    plot_importance_group_by(AX, DF, 'a')
    FIG.savefig('out.pdf', dpi=100, bbox_inches='tight')
    # output
    for instance, importance in zip(INSTANCES, IMPORTANCES):
        print(instance)
        print(importance)
        print(compute_groups(importance), '\n')
    MEANS = np.round(np.mean(IMPORTANCES, axis=0), decimals=2)
    STDS = np.round(np.std(IMPORTANCES, axis=0), decimals=2)
    print('MEANS:', MEANS)
    print('STD  :', STDS)
    print(compute_groups(MEANS))
    # print(len(list(filter(lambda x: x<2, MEANS))))
