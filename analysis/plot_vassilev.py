#!/usr/bin/env python3
''' vassilev plot and metrics '''

import sys
import pandas
import matplotlib.pyplot as plt

def vassilev_str(d, e):
    ''' vassilev string '''
    if d < -e:
        return -1
    if d <= e:
        return 0
    return 1

if __name__ == '__main__':
    # args
    INPUT_FILE = sys.argv[1]
    EPSILON = float(sys.argv[2])
    # compute
    DF = pandas.read_csv(INPUT_FILE, sep=';')
    VASSILEV = [vassilev_str(d, EPSILON) for d in DF.score.diff()[1:]]
    # plot
    FIG = plt.figure(dpi=100, figsize=(8, 8))
    AX = FIG.add_subplot(111)
    AX.plot(VASSILEV, drawstyle='steps-mid')
    AX.set_title('Vassilev')
    AX.grid()
    FIG.savefig('out.pdf', dpi=100, bbox_inches='tight')
