#!/usr/bin/env python3
''' optisig simulated annealing main functions '''

import copy
import numpy as np

import optisig.algos.mutator as mut
import optisig.utils.logger as logs

# Mutations
#######################################

MUTATION_OPS = [mut.mutate_cycle,
                mut.mutate_offset,
                mut.mutate_phase]

def mutate(solution, pmops, nmut):
    ''' mutate a solution '''
    systems = np.random.choice(len(solution.systems), nmut, replace=False)
    for s in systems:
        mop = np.random.choice(MUTATION_OPS, p=pmops)
        mop(solution.systems[s])
    return systems

# Survival selection
#######################################

def select_survival(solution, neighbour, t):
    ''' select the best solution '''
    delta = neighbour.score - solution.score
    p = min(1, np.exp(-(delta/t)))
    if np.random.rand() < p:
        return neighbour
    return solution

# Main loop
#######################################

def run(simulator, solution, g, steps, t, pmops, nmut):
    ''' run steps*n iterations '''
    for s in range(steps):
        for ng in range(g):
            neighbour = copy.deepcopy(solution)
            mutants = mutate(neighbour, pmops, nmut)
            simulator.run(neighbour)
            solution = select_survival(solution, neighbour, t)
            logs.log_solution(s*g+ng, solution, mutants, simulator.timer)
        t *= 0.9
