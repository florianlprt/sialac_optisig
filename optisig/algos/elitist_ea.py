#!/usr/bin/env python3
''' optisig elitist ea main functions '''

import copy
import numpy as np

import optisig.algos.mutator as mut
import optisig.utils.logger as logs

# Mutations
#######################################

MUTATION_OPS = [mut.mutate_cycle,
                mut.mutate_offset,
                mut.mutate_phase]

def mutate(solution, pmops, nmut):
    ''' mutate a solution '''
    systems = np.random.choice(len(solution.systems), nmut, replace=False)
    for s in systems:
        mop = np.random.choice(MUTATION_OPS, p=pmops)
        mop(solution.systems[s])
    return systems

# Crossover
#######################################

def crossover(parent1, parent2, pcross):
    ''' one point crossover '''
    child1 = copy.deepcopy(parent1)
    child2 = copy.deepcopy(parent2)
    if np.random.rand() < pcross:
        cross = np.random.randint(1, len(parent1.systems))
        (child1.systems,
         child2.systems) = (child1.systems[:cross] + child2.systems[cross:],
                            child2.systems[:cross] + child1.systems[cross:])
    return child1, child2

# Parents selection
#######################################

def binary_tournament(solutions):
    ''' binary tournament '''
    choice = np.random.choice(solutions, 2, replace=False)
    return max(choice, key=lambda x: x.score)

def select_parents(solutions, noffspring):
    ''' select parents for reproduction '''
    return [(binary_tournament(solutions), binary_tournament(solutions))
            for _ in range(noffspring)]

def select_survivals(solutions, children, npopulation, nelite):
    ''' select survivals '''
    solutions = sorted(solutions, key=lambda x: x.score)
    survivals = sorted(solutions[:nelite] + children, key=lambda x: x.score)
    return survivals[:npopulation]

# Main loop
#######################################

def run(simulator, solutions, g, npopulation, noffspring, nelite, pcross, pmops, nmut):
    ''' run g generations '''
    for ng in range(g):
        # parent selection
        parents = select_parents(solutions, noffspring)
        # recombination
        children = list(sum([crossover(parent1, parent2, pcross)
                             for parent1, parent2 in parents], ()))
        # mutation and evaluation
        for child in children:
            mutate(child, pmops, nmut)
            simulator.run(child)
        # survival selection
        solutions = select_survivals(solutions, children, npopulation, nelite)
        logs.log_solution((ng+1)*noffspring*2, solutions[0], [], simulator.timer)
