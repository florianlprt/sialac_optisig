#!/usr/bin/env python3
''' optisig hill climber main functions '''

import copy
import numpy as np

import optisig.algos.mutator as mut
import optisig.utils.logger as logs

# Mutations
#######################################

MUTATION_OPS = [mut.mutate_cycle,
                mut.mutate_offset,
                mut.mutate_phase]

def mutate(solution, pmops, nmut):
    ''' mutate a solution '''
    systems = np.random.choice(len(solution.systems), nmut, replace=False)
    for s in systems:
        mop = np.random.choice(MUTATION_OPS, p=pmops)
        mop(solution.systems[s])
    return systems

# Survival selection
#######################################

def select_survival(solution, neighbour):
    ''' select the best solution '''
    return min(solution, neighbour, key=lambda x: x.score)

# Main loop
#######################################

def run(simulator, solution, g, pmops, nmut):
    ''' run g iterations '''
    for ng in range(g):
        neighbour = copy.deepcopy(solution)
        mutants = mutate(neighbour, pmops, nmut)
        simulator.run(neighbour)
        solution = select_survival(solution, neighbour)
        logs.log_solution(ng, solution, mutants, simulator.timer)
