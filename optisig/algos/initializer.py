#!/usr/bin/env python3
''' optisig solution initializer '''

import numpy as np

import optisig.solution as sol
import optisig.settings as sets

import optisig.utils.parser as parser

# Initialization
#######################################

def initialize_system(cycle, offset=0):
    ''' initialize signal system controls according to a cycle '''
    phases = [cycle / sets.TOTAL_PHASES] * sets.TOTAL_PHASES
    return sol.System(cycle, offset, phases)

# Initialization methods
#######################################

def init_nth_sol(nth, total_population, nsystems):
    ''' initialize the nth solution of a population (same cycle for systems) '''
    step = sets.DELTA_CYCLE / total_population
    cycle = nth * step + sets.MIN_CYCLE
    systems = [initialize_system(cycle)
               for _ in range(nsystems)]
    return sol.Solution(systems)

def init_rand_sol(nsystems):
    ''' initialize a solution with random cycles '''
    systems = []
    for _ in range(nsystems):
        cycle = np.random.randint(sets.MIN_CYCLE, sets.MAX_CYCLE + 1)
        offset = np.random.randint(sets.MIN_OFFSET, sets.MAX_OFFSET + 1)
        systems.append(initialize_system(cycle, offset))
    return sol.Solution(systems)

def init_sol_with_cycle(cycle, nsystems):
    ''' initialize a solution with a cycle (same for all systems) '''
    systems = [initialize_system(cycle)
               for _ in range(nsystems)]
    return sol.Solution(systems)

def init_from(csv):
    ''' initialize a solution from csv '''
    loci = csv.split(';')
    return parser.log_to_sol(0, *loci)
