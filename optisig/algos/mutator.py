#!/usr/bin/env python3
''' optisig mutations main functions '''

import numpy as np

import optisig.settings as sets

# Deltas
#######################################

def generate_delta(maxi, signed=False):
    ''' generate a delta in [1, maxi] (or in [-maxi, -1] U [1, maxi] if specified) '''
    sign = (-1) ** np.random.randint(2) if signed else 1
    delta = np.random.randint(1, maxi+1)
    return sign * delta

def check_delta(delta, target, mini, maxi):
    ''' check and calibrate delta '''
    if target + delta < mini:
        return mini - target
    if target + delta > maxi:
        return maxi - target
    return delta

# Mutation operators
#######################################

def mutate_cycle(system, step=sets.STEP_CYCLE_MUTATION):
    ''' mutate cycle '''
    delta = generate_delta(step, signed=True)
    delta_phases = delta / sets.TOTAL_PHASES
    for phase in system.phases:
        delta_phases = check_delta(delta_phases, phase, sets.MIN_PHASE, sets.MAX_PHASE)
        delta = delta_phases * sets.TOTAL_PHASES
    delta = check_delta(delta, system.cycle, sets.MIN_CYCLE, sets.MAX_CYCLE)
    delta_phases = delta / sets.TOTAL_PHASES
    system.cycle += delta
    system.phases += delta_phases

def mutate_phase(system, step=sets.STEP_PHASE_MUTATION):
    ''' mutate phase '''
    delta = generate_delta(step, signed=True)
    delta_p0 = check_delta(delta, system.phases[0], sets.MIN_PHASE, sets.MAX_PHASE)
    delta_p1 = check_delta(-delta, system.phases[1], sets.MIN_PHASE, sets.MAX_PHASE)
    delta = min(delta_p0, delta_p1, key=np.abs)
    if delta == delta_p0:
        system.phases[0] += delta
        system.phases[1] -= delta
    else:
        system.phases[0] -= delta
        system.phases[1] += delta

def mutate_offset(system, step=sets.STEP_OFFSET_MUTATION):
    ''' mutate offset '''
    delta = generate_delta(step, signed=True)
    delta = check_delta(delta, system.offset, sets.MIN_OFFSET, sets.MAX_OFFSET)
    system.offset += delta
