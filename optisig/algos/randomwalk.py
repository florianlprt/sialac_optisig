#!/usr/bin/env python3
''' optisig random walk main functions '''

import copy
import numpy as np

import optisig.algos.mutator as mut
import optisig.utils.logger as logs

# Mutations
#######################################

MUTATION_OPS = [mut.mutate_cycle,
                mut.mutate_offset,
                mut.mutate_phase]

def mutate(solution, pmops, nmut):
    ''' mutate an solution '''
    systems = np.random.choice(len(solution.systems), nmut, replace=False)
    for s in systems:
        mop = np.random.choice(MUTATION_OPS, p=pmops)
        mop(solution.systems[s])
    return systems

# Main loop
#######################################

def run(simulator, solution, g, pmops, nmut):
    ''' run g iterations '''
    trial = hash(solution)
    trials = [trial]
    for ng in range(g):
        while trial in trials:
            neighbour = copy.deepcopy(solution)
            mutants = mutate(neighbour, pmops, nmut)
            trial = hash(neighbour)
        trials.append(trial)
        simulator.run(neighbour)
        solution = neighbour
        logs.log_solution(ng, solution, mutants, simulator.timer)
