#!/usr/bin/env python3
''' optisig UCB  main functions '''

import copy
import numpy as np

import optisig.algos.mutator as mut
import optisig.utils.logger as logs

# Mutations
#######################################

MUTATION_OPS = [mut.mutate_cycle,
                mut.mutate_offset,
                mut.mutate_phase]

def mutate(solution, groups, igroup, pmops, nmut):
    ''' mutate a systems group of a solution '''
    glen = len(groups[igroup])
    nmut = glen if nmut > glen else nmut
    group = np.random.choice(groups[igroup], nmut, replace=False)
    for _ in group:
        mop = np.random.choice(MUTATION_OPS, 1, p=pmops)[0]
        mop(solution.systems[_])
    return group

# UCB scoring functions
#######################################

def score_ucb1(i, sums, visits, k, total, init_evals, **_):
    ''' compute ucb score (exploitation = mean)'''
    v = visits[i]
    if v >= init_evals:
        s = sums[i]
        exploitation = (s / v) / np.sqrt(v)
        exploration = np.sqrt(np.log(total) / v)
        return exploitation + k * exploration
    return np.inf

def score_tuned(i, sums, sums2, visits, k, total, init_evals, **_):
    ''' compute ucb score (ucb tuned) '''
    v = visits[i]
    if v >= init_evals:
        s = sums[i]
        s2 = sums2[i]
        exploitation = (s / v) / np.sqrt(v)
        exploration = (s2 / v) - (s / v)**2 + np.sqrt(2 * np.log(total) / v)
        return exploitation + k * exploration
    return np.inf

SCORINGS_D = {
    'ucb1': score_ucb1,
    'tuned': score_tuned
}

# UCB reward functions
#######################################

def reward_abs(current, neighbour):
    ''' compute reward (absolute) '''
    return abs(current - neighbour)

def reward_liu(current, neighbour):
    ''' compute reward (liu) '''
    return max(0, current - neighbour)

REWARDINGS_D = {
    'abs': reward_abs,
    'liu': reward_liu,
}

# UCB data
#######################################

def init(init_evals, k, ngroups, scoring, rewarding):
    ''' initialize ucb state '''
    return {
        'init_evals': init_evals,
        'k': k,
        'ngroups': ngroups,
        'scoring': scoring,
        'rewarding': rewarding,
        'scores': [0] * ngroups,
        'sums': [0] * ngroups,
        'sums2': [0] * ngroups,
        'visits': [0] * ngroups,
        'total': 0
    }

# UCB functions
#######################################

def update(ucb, group, s1, s2):
    ''' update ucb '''
    reward = REWARDINGS_D[ucb['rewarding']]
    score = SCORINGS_D[ucb['scoring']]
    ucb['total'] += 1
    ucb['sums'][group] += reward(s1, s2)
    ucb['sums2'][group] += reward(s1, s2)**2
    ucb['visits'][group] += 1
    ucb['scores'] = [score(_, **ucb) for _ in range(ucb['ngroups'])]

# Survival selection
#######################################

def select_survival(solution, neighbour):
    ''' select the best solution '''
    return min(solution, neighbour, key=lambda x: x.score)

# Main loop
#######################################

def run(simulator, solution, g, pmops, nmut, k, init_evals, groups, scoring, rewarding):
    ''' run ucbhc '''
    ucb = init(init_evals, k, len(groups), scoring, rewarding)
    for ng in range(g):
        igroup = np.argmax(ucb['scores'])
        neighbour = copy.deepcopy(solution)
        mutants = mutate(neighbour, groups, igroup, pmops, nmut)
        simulator.run(neighbour)
        update(ucb, igroup, solution.score, neighbour.score)
        solution = select_survival(solution, neighbour)
        logs.log_solution(ng, solution, mutants, simulator.timer, ucb)
