#!/usr/bin/env python3
''' optisig main function '''

import ast
import numpy as np

import optisig.algos.initializer as init
import optisig.algos.randomwalk as rw
import optisig.algos.hillclimber as hc
import optisig.algos.annealing as sa
import optisig.algos.ucb as ucb
import optisig.algos.elitist_ea as eea

import optisig.utils.logger as logs

import optisig.settings as sets

# Random walk
#######################################

def optim_rw(simulator,
             nsys=sets.NSYS,
             g=sets.G,
             pmops=sets.P_MUTATION_OPS,
             nmut=sets.N_MUTATIONS,
             **_):
    ''' random walk '''
    solution = init.init_rand_sol(nsys)
    simulator.run(solution)
    rw.run(simulator, solution, g, pmops, nmut)

# Hill climber
#######################################

def optim_hc(simulator,
             nsys=sets.NSYS,
             g=sets.G,
             pmops=sets.P_MUTATION_OPS,
             nmut=sets.N_MUTATIONS,
             **_):
    ''' hill climber '''
    solution = init.init_rand_sol(nsys)
    simulator.run(solution)
    hc.run(simulator, solution, g, pmops, nmut)

# Simulated annealing
#######################################

def optim_sa(simulator,
             nsys=sets.NSYS,
             g=sets.G,
             steps=sets.SA_STEPS,
             t=sets.SA_TEMPERATURE,
             pmops=sets.P_MUTATION_OPS,
             nmut=sets.N_MUTATIONS,
             **_):
    ''' simulated annealing '''
    solution = init.init_rand_sol(nsys)
    simulator.run(solution)
    sa.run(simulator, solution, g, steps, t, pmops, nmut)

# UCB
#######################################

def optim_ucb(simulator,
              nsys=sets.NSYS,
              g=sets.G,
              pmops=sets.P_MUTATION_OPS,
              nmut=sets.N_MUTATIONS,
              k=sets.K,
              init_evals=sets.INIT_EVALS,
              grouping=sets.DEFAULT_GROUPING,
              scoring=sets.DEFAULT_SCORING,
              rewarding=sets.DEFAULT_REWARDING,
              **_):
    ''' upper confidence bound + hc '''
    groups = sets.GROUPS_D[grouping]
    solution = init.init_rand_sol(nsys)
    simulator.run(solution)
    ucb.run(simulator, solution, g, pmops, nmut, k, init_evals, groups, scoring, rewarding)

# Elitist EA
#######################################

def optim_eea(simulator,
              nsys=sets.NSYS,
              g=sets.G,
              npopulation=sets.EA_POPULATION,
              noffspring=sets.EA_OFFSPRING,
              nelite=sets.EA_ELITE,
              pcross=sets.P_CROSS,
              pmops=sets.P_MUTATION_OPS,
              nmut=sets.N_MUTATIONS,
              **_):
    ''' elitist ea '''
    solutions = [init.init_rand_sol(nsys) for _ in range(npopulation)]
    for sol in solutions:
        simulator.run(sol)
    eea.run(simulator, solutions, g, npopulation, noffspring, nelite, pcross, pmops, nmut)

# Launcher
#######################################

OPTIMIZERS_D = {
    'rw': optim_rw,
    'hc': optim_hc,
    'sa': optim_sa,
    'ucb': optim_ucb,
    'eea': optim_eea
}

def optim(simulator, logdir, algo, restarts=1, random_seed=0, **args):
    ''' launcher '''
    # init random seeds
    np.random.seed(random_seed)
    seeds = np.random.randint(0, 1e9, restarts)
    # restarts
    for _ in range(restarts):
        # init
        logs.init_logs(logdir)
        # set random seed
        seed = seeds[_]
        np.random.seed(seed)
        simulator.randomseed = seed
        # run
        OPTIMIZERS_D[algo](simulator, **args)
        # end
        logs.close_logs()
