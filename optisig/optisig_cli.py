#!/usr/bin/env python3
''' MATSim optimization algorithms for signal controls optimization '''

import sys

import optisig.optisig as optisig

import optisig.utils.parser as parser
import optisig.utils.simulator as sim

if __name__ == '__main__':

    # command line arguments
    if len(sys.argv) != 5:
        print('usage: ', sys.argv[0], '<algo> <algo params> <simu params> <log directory>')
        sys.exit(-1)

    # command line arguments
    ALGO_NAME = sys.argv[1]
    ALGO_STR = sys.argv[2]
    SIMU_STR = sys.argv[3]
    LOG_DIR = sys.argv[4]

    ALGO_PARAMS = parser.parse_params(ALGO_STR)
    SIMU_PARAMS = parser.parse_params(SIMU_STR)

    # simulator
    SIMULATOR = sim.Simulator(**SIMU_PARAMS)

    # run
    optisig.optim(SIMULATOR, LOG_DIR, ALGO_NAME, **ALGO_PARAMS)
