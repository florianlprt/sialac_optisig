#!/usr/bin/env python3
''' optisig solution classes '''

import numpy as np

class System:
    ''' class definition for a signal system controls '''
    def __init__(self, cycle, offset, phases):
        self.cycle = cycle
        self.offset = offset
        self.intergreen = 3
        self.phases = np.array(phases)

    def __str__(self):
        out = ("{{'cycle':{}, 'offset':{}, 'phases':{}}}"
               .format(self.cycle, self.offset, self.phases))
        return out

    def __repr__(self):
        out = ("{{'cycle':{}, 'offset':{}, 'phases':{}}}"
               .format(self.cycle, self.offset, self.phases))
        return out

class Solution:
    ''' class definition for a solution '''
    def __init__(self, systems, score=0, fitness='tripduration'):
        self.score = float(score)
        self.fitness = fitness
        self.systems = systems

    def __str__(self):
        out = ("{{'fitness':{}, 'score':{}, 'systems':{}}}"
               .format(self.fitness, self.score, self.systems))
        return out

    def __repr__(self):
        out = ("{{'fitness':{}, 'score':{}, 'systems':{}}}"
               .format(self.fitness, self.score, self.systems))
        return out

    def __hash__(self):
        return hash(repr(self))
