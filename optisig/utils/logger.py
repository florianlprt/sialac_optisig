#!/usr/bin/env python3
''' optisig log system '''

import logging
import time
import datetime

LOGS_PATH = 'logs/{}/log_{}.csv'
ALL_LOGS_PATHS = 'logs/{}/*.csv'

# Setup
#######################################

def setup_logger(name, log_file, level=logging.INFO):
    ''' setup as many loggers as you want '''
    handler = logging.FileHandler(log_file, mode='a')
    logger = logging.getLogger(name)
    logger.setLevel(level)
    logger.addHandler(handler)
    return logger

def setup_logs(logdir, instant, header=False):
    ''' setup optisig logs '''
    log_filename = LOGS_PATH.format(logdir, instant)
    logger = setup_logger('optisig', log_filename)
    if header:
        logger.info('generation;score;mutants;cycles;offsets;phases0;phases1;time;simtime;algostate')

# Init
#######################################

def init_logs(logdir):
    ''' init logs '''
    instant = datetime.datetime.now().strftime('%Y%m%d%H%M%S%f')
    setup_logs(logdir, instant, header=True)

# Close
#######################################

def close_logs():
    ''' close logs '''
    logger = logging.getLogger('optisig')
    logger.handlers = []

# Messages
#######################################

def log_solution(generation, solution, mutants, simtime, algostate=''):
    ''' log '''
    logger = logging.getLogger('optisig')
    logger.info('{};{};{};{};{};{};{};{};{};{}'.format(
        generation,
        solution.score,
        ','.join([str(_) for _ in mutants]),
        ','.join([str(system.cycle) for system in solution.systems]),
        ','.join([str(system.offset) for system in solution.systems]),
        ','.join([str(system.phases[0]) for system in solution.systems]),
        ','.join([str(system.phases[1]) for system in solution.systems]),
        time.time(),
        simtime,
        algostate))
