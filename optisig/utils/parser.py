#!/usr/bin/env python3
''' optisig xml parsers '''

import numpy as np
import pandas as pd
import lxml.etree as etree

import optisig.solution as sol

# Command line arguments
#######################################

def parse_value(string):
    ''' convert string to int, float or string '''
    try:
        return int(string)
    except ValueError:
        try:
            return float(string)
        except ValueError:
            return string

def parse_params(param_str):
    ''' parse a param string to a dict '''
    dict_params = {}
    if param_str:
        for key_value_str in param_str.split(','):
            key, value = key_value_str.split('=')
            if key in ['pmops']:
                dict_params[key] = np.fromstring(str(value), dtype=float, sep=':')
            else:
                dict_params[key] = parse_value(value)
    return dict_params

# XML utils
#######################################

def make_subelement(parent_node, sub_name, sub_attrs=None):
    ''' creates an xml sub element and set its attributes '''
    sub = etree.SubElement(parent_node, sub_name)
    if sub_attrs is None:
        return sub
    for attr, value in sub_attrs.items():
        sub.set(attr, value)
    return sub

def count_systems(xml_input):
    ''' count signal systems in a file '''
    root = etree.parse(xml_input)
    count = root.xpath('count(//x:signalSystem)', namespaces={'x': 'http://www.matsim.org/files/dtd'})
    return int(count)

# XML -> Solution
#######################################

def make_phases(groups, intergreen):
    ''' make list of green times from XML signal groups '''
    phases = []
    for g in groups:
        onset = g.find('./{*}onset').get('sec')
        dropping = g.find('./{*}dropping').get('sec')
        phases.append(int(dropping) - int(onset) + intergreen)
    return phases

def make_systems(plans):
    ''' make list of Signal from XML plans '''
    systems = []
    for p in plans:
        cycle = p.find('./{*}cycleTime').get('sec')
        offset = p.find('./{*}offset').get('sec')
        intergreen = 5
        groups = p.findall('./{*}signalGroupSettings')
        phases = make_phases(groups, intergreen)
        system = sol.System(int(cycle), int(offset), phases)
        systems.append(system)
    return systems

def xml_to_sol(xml_input):
    ''' translate XML signal control file into a Solution '''
    root = etree.parse(xml_input)
    plans = root.findall('//{*}signalPlan')
    systems = make_systems(plans)
    return sol.Solution(systems)

# Solution -> XML
#######################################

def make_header(root):
    ''' make xml document header '''
    root.set('xmlns', 'http://www.matsim.org/files/dtd')
    root.set('{http://www.w3.org/2001/XMLSchema-instance}schemaLocation', 'http://www.matsim.org/files/dtd http://www.matsim.org/files/dtd/signalControl_v2.0.xsd')

def make_controller(signal_system):
    ''' make signal system controller '''
    controller = make_subelement(signal_system, 'signalSystemController')
    controller_identifier = make_subelement(controller, 'controllerIdentifier')
    controller_identifier.text = 'DefaultPlanbasedSignalSystemController'
    return controller

def make_start_stop_cycle_offset(plan, solution):
    ''' make global signal controls parameters '''
    make_subelement(plan, 'start', {'daytime': '06:00:00'})
    make_subelement(plan, 'stop', {'daytime': '18:00:00'})
    make_subelement(plan, 'cycleTime', {'sec': str(int(solution.cycle))})
    make_subelement(plan, 'offset', {'sec': str(int(solution.offset))})

def make_groups(plan, system):
    ''' make groups settings for each signal in a signal system '''
    # phase 1
    dropping = int(system.phases[0] - system.intergreen)
    settings = make_subelement(plan, 'signalGroupSettings', {'refId': '1'})
    make_subelement(settings, 'onset', {'sec': '0'})
    make_subelement(settings, 'dropping', {'sec': str(dropping)})
    # phase 2
    dropping = int(system.phases[0] + system.phases[1] - system.intergreen)
    settings = make_subelement(plan, 'signalGroupSettings', {'refId': '2'})
    make_subelement(settings, 'onset', {'sec': str(int(system.phases[0]))})
    make_subelement(settings, 'dropping', {'sec': str(dropping)})

def make_plans(root, solution):
    ''' make a signal plan for each signals systems of a solution '''
    for n, s in enumerate(solution.systems):
        signal_system = make_subelement(root, 'signalSystem', {'refId': str(n+1)})
        signal_controller = make_controller(signal_system)
        signal_plan = make_subelement(signal_controller, 'signalPlan', {'id': '1'})
        make_start_stop_cycle_offset(signal_plan, s)
        make_groups(signal_plan, s)

def sol_to_xml(solution, xml_output):
    ''' make signal control xml root '''
    root = etree.Element('signalControl')
    make_header(root)
    make_plans(root, solution)
    tree = etree.ElementTree(root)
    tree.write(xml_output, pretty_print=True, xml_declaration=True, encoding="utf-8")

# Log => Solution
#######################################
def log_to_sol(score, cycles, offsets, phases0, phases1):
    ''' remake a solution from logs '''
    cy  = np.fromstring(cycles, dtype=float, sep=',')
    off = np.fromstring(offsets, dtype=float, sep=',')
    ph1 = np.fromstring(phases0, dtype=float, sep=',')
    ph2 = np.fromstring(phases1, dtype=float, sep=',')
    sys = [sol.System(c, o, [p1, p2]) for c, o, p1, p2 in zip(cy, off, ph1, ph2)]
    return sol.Solution(sys, score=score)

# Loading from logs
#######################################
def load_from_logs(len_logs, last_log):
    ''' foo '''
    df = pd.read_csv(last_log, sep=';').tail(1)
    if df.empty:
        return { 'r': len_logs - 1 }
    return {
        'r': len_logs - 1,
        'g': int(df.generation.item()) + 1,
        'solution': log_to_sol(
            df.score.item(),
            df.cycles.item(),
            df.offsets.item(),
            df.phases0.item(),
            df.phases1.item()),
        'state': df.algostate.item() }

# Fitness : trip duration
#######################################

TRIPDURATIONS_F = '{}{}.tripdurations.txt'

def parse_tripduration(output, nb_evals):
    ''' get trip duration (in seconds) from a MATSim output file '''
    fname = TRIPDURATIONS_F.format(output, nb_evals)
    with open(fname, 'r') as f:
        for last in f:
            pass
        seconds = last.split(' ')[3]
        return float(seconds)
