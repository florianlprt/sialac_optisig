#!/usr/bin/env python3
''' optisig simulator '''

import os
import time

import optisig.settings as sets
import optisig.utils.parser as parser

class Simulator:
    ''' simulator class definition '''

    def __init__(self,
                 env,
                 config=sets.SIM_CONFIG,
                 plans=sets.SIM_PLANS,
                 output=sets.SIM_OUTPUT,
                 lastiter=sets.SIM_LASTITER,
                 usesignals=sets.SIM_USESIGNALS,
                 usetransit=sets.SIM_USETRANSIT):
        self.env = env + '/'
        self.config = self.env + config
        self.plans = plans
        self.output = self.env + output
        self.lastiter = lastiter
        self.usesignals = usesignals
        self.usetransit = usetransit
        self.randomseed = sets.SIM_RANDOMSEED
        self.iters = self.output + sets.SIM_ITERS.format(self.lastiter)
        self.timer = 0

    def sim(self, solution):
        ''' simulate a solution '''
        parser.sol_to_xml(solution, self.env + sets.SIGNALS_CONTROL_XML)
        os.system(sets.SIM_CMD.format(self.config,
                                      self.plans,
                                      self.output,
                                      self.lastiter,
                                      self.usesignals,
                                      self.usetransit,
                                      self.randomseed))

    def run(self, solution):
        ''' run the simulator '''
        chrono = time.time()
        self.sim(solution)
        self.timer = time.time() - chrono
        fitness = sets.FITNESS_D[solution.fitness]
        solution.score = fitness(self.iters, self.lastiter)
