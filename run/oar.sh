#!/bin/sh

#OAR --array-param-file params.txt
#OAR -l /core=1,walltime=24:00:00
#OAR -p host="orval01"
#OAR -t besteffort
#OAR -t idempotent

. ~/opt/venvs/optisig/bin/activate

./run.sh $@
