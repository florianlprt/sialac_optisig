#!/bin/sh

CLI="optisig_cli.py"

# command line arguments

if [ $# -lt 7 ]; then
    echo "usage: ./run.sh <scenario> <agents> <home> <work> <signals> <algo> <algo_params> [logs_dir]"
    exit
fi

SCENARIO=$1
PLANS=$2_$3_$4
SIGNALS=$5
ALGO=$6
ALGO_PARAMS=$7
LOGS=$8

# make simulation environment
if [ -z $OAR_JOB_ID ]; then OAR_JOB_ID=$RANDOM; fi
if [ -z $LOGS ]; then LOGS=${ALGO}_${SCENARIO}_${PLANS}_${SIGNALS}; fi

SRC="./matsim/scenarios/${SCENARIO}"
ENV="./matsim/simulations/${SCENARIO}_${OAR_JOB_ID}"
SIMU_PARAMS="env=${ENV}/,usesignals=true"

cd ..
mkdir -p logs/${LOGS}
mkdir -p ${ENV}
cp ${SRC}/*xml ${ENV}
cp ${SRC}/pt/*.xml ${ENV}
cp ${SRC}/plans/plans_${SCENARIO}_${PLANS}_equil.xml.gz ${ENV}/plans.xml.gz
cp ${SRC}/signals/signals_systems_${SIGNALS}.xml ${ENV}/signals_systems.xml
cp ${SRC}/signals/signals_groups_${SIGNALS}.xml ${ENV}/signals_groups.xml

# run
$CLI $ALGO $ALGO_PARAMS $SIMU_PARAMS $LOGS
