import pkgconfig
from setuptools import setup

setup(
    name = 'sialac_optisig',
    version = '0.1',
    scripts = ['optisig/optisig_cli.py'],
    packages = [
        'optisig',
        'optisig/algos',
        'optisig/utils'
    ],
    install_requires = [
        'lxml',
        'matplotlib',
        'numpy',
        'pandas',
        'scipy',
        'statsmodels'
    ],
)
